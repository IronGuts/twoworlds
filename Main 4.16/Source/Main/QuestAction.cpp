// Fill out your copyright notice in the Description page of Project Settings.

#include "QuestAction.h"

UQuestAction::UQuestAction()
{
}

UQuestAction::~UQuestAction()
{
}





void UQuestAction::Init(const FString &actionType, const FString &target,
	int numberOfTargets, int numberOfDoneTargets,
	bool mandatory, bool done)
{
	this->actionType = actionType;
	this->target = target;
	this->numberOfTargets = numberOfTargets;
	this->numberOfDoneTargets = numberOfDoneTargets;
	this->mandatory = mandatory;
	this->done = done;
}
