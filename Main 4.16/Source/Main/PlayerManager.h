// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "Profile.h"
#include "AdaptativeGameSettings.h"


#include "PlayerManager.generated.h"


#define private public
#define protected public

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MAIN_API UPlayerManager : public UActorComponent
{
	GENERATED_BODY()
	
	public:
		
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
		AAdaptativeGameSettings *settings;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Profile")
		UProfile *profile;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		TArray<UQuest*> quests;

	public:	
		// Sets default values for this component's properties
		UPlayerManager();

		UFUNCTION(BlueprintCallable, Category = "Actions")
		void OnActionDone(const FString &action, const FString &target);

		UFUNCTION(BlueprintCallable, Category = "Quests")
		void OnQuestDone(UQuest *target);

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;

	public:	
		// Called every frame
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
