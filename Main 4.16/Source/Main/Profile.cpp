// Fill out your copyright notice in the Description page of Project Settings.

#include "Profile.h"

#include "AdaptativeGameSettings.h"

UProfile::UProfile()
{
}

UProfile::~UProfile()
{
}


void UProfile::applyChange(const UProfile *profile)
{
	int imax = (axes.Num() < profile->axes.Num() ? axes.Num() : profile->axes.Num());
	for (int i = 0; i < imax; ++i)
		axes[i] *= profile->axes[i];
}


float UProfile::GetMinAxisValue(const AAdaptativeGameSettings *settings)
{
	return settings->minAxisValue;
}

void UProfile::SetMinAxisValue(AAdaptativeGameSettings *settings, float value)
{
	if(value > 0.0f)
	settings->minAxisValue = value;
}


const TArray<float> &UProfile::GetValues() const
{
	return axes;
}

void UProfile::SetValues(const TArray<float> &values)
{
	axes = values;
}

int UProfile::GetIndex(const AAdaptativeGameSettings *settings, const FString &axis) const
{
	return settings->axes.IndexOfByKey(axis);
}

bool UProfile::HasAxis(const AAdaptativeGameSettings *settings, const FString &axis) const
{
	return GetIndex(settings, axis) != INDEX_NONE;
}

float UProfile::GetValue(int index) const
{
	return axes[index];
}

float UProfile::GetAxisValue(const AAdaptativeGameSettings *settings, const FString &axis) const
{
	return axes[GetIndex(settings, axis)];
}

float &UProfile::GetValueRef(int index)
{
	return axes[index];
}

float &UProfile::GetAxisValueRef(const AAdaptativeGameSettings *settings, const FString &axis)
{
	return axes[GetIndex(settings, axis)];
}

void UProfile::SetValue(int index, float value)
{
	axes[index] = value;
}

void UProfile::SetAxisValue(const AAdaptativeGameSettings *settings, const FString &axis, float value)
{
	axes[GetIndex(settings, axis)] = value;
}

void UProfile::AddAxisWithValue(float value)
{
	axes.Add(value);
}

void UProfile::AddAxis()
{
	AddAxisWithValue(1.0f / sqrtf(axes.Num() + 1.0f));
} 

void UProfile::RemoveAxis(int index)
{
	axes.Remove(index);
}

void UProfile::RemoveNamedAxis(AAdaptativeGameSettings *settings, const FString &axis)
{
	RemoveAxis(GetIndex(settings, axis));
}

float UProfile::Magnitude() const
{
	float mag = 0.0f;
	for (int i = 0; i < axes.Num(); ++i)
		mag += powf(axes[i], 2.0f);

	return sqrtf(mag);
}

void UProfile::Normalize()
{
	operator/=(Magnitude());
}

bool UProfile::isNormalized() const
{
	return Magnitude() == 1.0f;
}

int UProfile::SelectAxis(float rand_value) const
{
	float accum = axes[0];
	int i = 0;
	while (rand_value > accum && i < axes.Num())
	{
		++i;
		accum += axes[i];
	}

	return i;
}

void UProfile::Check(const AAdaptativeGameSettings *settings)
{
	float minAxisValue = settings->minAxisValue;
	for (int i = 0, imax = axes.Num(); i < imax; ++i)
	{
		if (fabs(axes[i]) < minAxisValue)
			axes[i] = minAxisValue * (axes[i] < -minAxisValue ? -1 : 1);
	}
}
