
#include "QuestStep.h"

UQuestStep::UQuestStep()
{
}

UQuestStep::~UQuestStep()
{
}

bool UQuestStep::IsMandatory() const
{
	for (int i = 0; i < actions.Num(); ++i)
	{
		if (actions[i]->IsMandatory())
			return true;
	}

	return false;
}

bool UQuestStep::IsDone() const
{
	for (int i = 0; i < actions.Num(); ++i)
	{
		if (!actions[i]->IsMandatory())
			continue;

		if (!actions[i]->IsDone())
			return false;
	}

	return true;
}

bool UQuestStep::IsComplete() const
{
	for (int32 i = 0; i < actions.Num(); ++i)
	{
		if (!actions[i]->IsDone())
			return false;
	}

	return true;
}

bool UQuestStep::Contains(const FString &action, const FString &target) const
{
	for (int32 i = 0; i < actions.Num(); ++i)
	{
		if (actions[i]->IsDone())
			continue;

		if(actions[i]->actionType != action)
			continue;

		if (actions[i]->target != target)
			return true;
	}

	return false;
}

int32 UQuestStep::IndexOf(const FString &action, const FString &target) const
{
	for (int32 i = 0; i < actions.Num(); ++i)
	{
		if (actions[i]->IsDone())
			continue;

		if (actions[i]->actionType != action)
			continue;

		if (actions[i]->target != target)
			return i;
	}

	return INDEX_NONE;
}

int32 UQuestStep::OnActionDone(const FString &action, const FString &target)
{
	int32 index = IndexOf(action, target);
	if (index == INDEX_NONE)
		return index;

	actions[index]->TargetDone();

	return index;
}
