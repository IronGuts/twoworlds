// Fill out your copyright notice in the Description page of Project Settings.
using System.IO;
using UnrealBuildTool;

public class Main : ModuleRules
{
    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/")); }
    }

    private string LuaVersion
    {
        get { return "5.3"; }
    }

    private string LuaLibraryPath
    {
        get { return Path.Combine(ThirdPartyPath, "Lua", "lib"); }
    }

    private string LuaIncludePath
    {
        get { return Path.Combine(ThirdPartyPath, "Lua", "include"); }
    }

    private string GetLuaLibFileName(UnrealTargetPlatform platform)
    {
        switch(platform)
        {
            case UnrealTargetPlatform.Win64:
                return "lua." + LuaVersion + ".x64.lib";
            case UnrealTargetPlatform.Win32:
                return "lua." + LuaVersion + ".x86.lib";
            default:
                return "";
        }
    }

    private bool IsLuaSupportedPlatform(UnrealTargetPlatform platform)
    {
        switch (platform)
        {
            case UnrealTargetPlatform.Win64:
            case UnrealTargetPlatform.Win32:
                return true;
            default:
                return false;
        }
    }

    private bool LoadLua(ReadOnlyTargetRules TargetRules)
    {
        if (!IsLuaSupportedPlatform(Target.Platform))
        {
            Definitions.Add(string.Format("WITH_LUA_BINDING={0}", 0));
            return false;
        }


        PublicAdditionalLibraries.Add(Path.Combine(LuaLibraryPath, GetLuaLibFileName(Target.Platform)));

        PublicIncludePaths.Add(LuaIncludePath);

        Definitions.Add(string.Format("WITH_LUA_BINDING={0}", 1));

        return true;
    }

    public Main(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;


        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

		PrivateDependencyModuleNames.AddRange(new string[] {  });

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true

        LoadLua(Target); // This functions loads Lua
    }
}
