// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerManager.h"

#include "Engine/World.h"


// Sets default values for this component's properties
UPlayerManager::UPlayerManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	AActor *act = GetOwner();
	if (act == nullptr)
		return;
	
	
	UWorld *world = act->GetWorld();
	if (world == nullptr)
		return;
	
	AWorldSettings *wset = world->GetWorldSettings(false, false);
	if (wset == nullptr)
		return;

	settings = static_cast<AAdaptativeGameSettings*>(wset);

	profile = NewObject<UProfile>();
	for (int32 i = 0, imax = settings->axes.Num(); i < imax; ++i)
		profile->axes.Add(1.0f);

	profile->Normalize();
	profile->Check(settings);

	// ...
}


// Called when the game starts
void UPlayerManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UPlayerManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


void UPlayerManager::OnActionDone(const FString &action, const FString &target)
{
	UProfile *action_profile = settings->actionProfiles[action];
	if (!action_profile)
		return;

	profile->applyChange(action_profile);
	profile->Normalize();
}

void UPlayerManager::OnQuestDone(UQuest *q)
{
	quests.Remove(q);
}
