// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/WorldSettings.h"


#include "LuaEngine.h"
#include "Profile.h"
#include "QuestTemplate.h"

#include "AdaptativeGameSettings.generated.h"

#define private public
#define protected public

/**
 * 
 */
UCLASS()
class MAIN_API AAdaptativeGameSettings : public AWorldSettings
{
	GENERATED_BODY()
	
	public:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Axes")
		TArray<FString> axes;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Axes")
		float minAxisValue;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Axes")
		ULuaEngine *luaEngine;
	
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actions")
		TMap<FString, UProfile*> actionProfiles;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		TArray<UQuestTemplate*> questTemplates;

	public:
		AAdaptativeGameSettings();
		~AAdaptativeGameSettings();

		UFUNCTION(BlueprintCallable, Category = "Axes")
		float GetMinAxisValue() const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void SetMinAxisValue(float value);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		int GetIndex(const FString &axis) const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		bool HasAxis(const FString &axis) const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void AddAxis(const FString &axis);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void RemoveAxis(const FString &axis);

		UFUNCTION(BlueprintCallable, Category = "Actions")
		UProfile *GetActionProfiles(const FString &action);

		UFUNCTION(BlueprintCallable, Category = "Actions")
		UQuest *GenerateQuest(const UProfile *profile);
};
