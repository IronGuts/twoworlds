// Fill out your copyright notice in the Description page of Project Settings.

#include "Quest.h"

UQuest::UQuest()
{
}

UQuest::~UQuest()
{
}

void UQuest::Init(const FString &questName, const TArray<UQuestStep*> &questSteps, int questCurrentStep, bool isMandatory)
{
	name = name;
	steps = steps;
	currentStep = currentStep;
	mandatory = mandatory;
}

bool UQuest::IsDone() const
{
	for (int i = 0; i < steps.Num(); ++i)
	{
		if (!steps[i]->IsDone())
			return false;
	}

	return true;
}

bool UQuest::HasActions() const
{
	for (int i = 0; i < steps.Num(); ++i)
	{
		if (!steps[i]->HasActions())
			return true;
	}

	return false;
}

bool UQuest::HasUndoneActions() const
{
	for (int i = 0; i < steps.Num(); ++i)
	{
		if (!steps[i]->IsComplete())
			return true;
	}

	return false;
}

bool UQuest::IsStepHasUndoneActions(int step) const
{
	return !steps[step]->IsComplete();
}

bool UQuest::IsValid() const
{
	return !name.IsEmpty() && HasActions();
}

void UQuest::Check()
{
	if (currentStep < steps.Num())
		currentStep += (steps[currentStep]->IsDone() ? 0 : 1);
}

void UQuest::OnActionDone(const FString &action, const FString &target)
{
	for (int32 i = currentStep; i < steps.Num(); ++i)
	{
		if (steps[i]->OnActionDone(action, target) != INDEX_NONE)
			return;
	}
}
