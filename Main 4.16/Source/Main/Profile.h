// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"

#include "Profile.generated.h"

#define private public
#define protected public

class AAdaptativeGameSettings;

UCLASS(BlueprintType)
class MAIN_API UProfile : public UObject
{
	GENERATED_BODY()

	public:

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Axes")
		TArray<float> axes;


	public:
		UProfile();
		~UProfile();

		UFUNCTION(BlueprintCallable, Category = "Axes")
			void applyChange(const UProfile *profile);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		float GetMinAxisValue(const AAdaptativeGameSettings *settings);
		
		UFUNCTION(BlueprintCallable, Category = "Axes")
		void SetMinAxisValue(AAdaptativeGameSettings *settings, float value);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		const TArray<float> &GetValues() const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void SetValues(const TArray<float> &values);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		int GetIndex(const AAdaptativeGameSettings *settings, const FString &axis) const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		bool HasAxis(const AAdaptativeGameSettings *settings, const FString &axis) const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		float GetValue(int index) const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		float GetAxisValue(const AAdaptativeGameSettings *settings, const FString &axis) const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		float &GetValueRef(int index);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		float &GetAxisValueRef(const AAdaptativeGameSettings *settings, const FString &axis);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void SetValue(int index, float value);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void SetAxisValue(const AAdaptativeGameSettings *settings, const FString &axis, float value);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void AddAxisWithValue(float value);
		
		UFUNCTION(BlueprintCallable, Category = "Axes")
		void AddAxis();

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void RemoveAxis(int index);

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void RemoveNamedAxis(AAdaptativeGameSettings *settings, const FString &axis);


		FORCEINLINE float operator[](int index) const
		{
			return GetValue(index);
		}
		
		FORCEINLINE float &operator[](int index)
		{
			return GetValueRef(index);
		}
		
		FORCEINLINE UProfile operator-() const
		{
			UProfile p;
			for (int i = 0; i < axes.Num(); ++i)
				p.axes[i] = 1.0f - axes[i];

			p.Normalize();

			return p;
		}

		FORCEINLINE UProfile &operator*=(float value)
		{
			for (int i = 0; i < axes.Num(); ++i)
				axes[i] *= value;

			return *this;
		}

		FORCEINLINE UProfile &operator*=(const TArray<float> &values)
		{
			int imax = (axes.Num() < values.Num() ? axes.Num() : values.Num());
			for (int i = 0; i < imax; ++i)
				axes[i] *= values[i];

			return *this;
		}

		FORCEINLINE UProfile &operator*=(const UProfile &profile)
		{
			int imax = (axes.Num() < profile.axes.Num() ? axes.Num() : profile.axes.Num());
			for (int i = 0; i < imax; ++i)
				axes[i] *= profile.axes[i];

			return *this;
		}

		FORCEINLINE UProfile &operator/=(float value)
		{
			for (int i = 0; i < axes.Num(); ++i)
				axes[i] *= value;

			return *this;
		}

		FORCEINLINE UProfile &operator/=(const TArray<float> &values)
		{
			int imax = (axes.Num() < values.Num() ? axes.Num() : values.Num());
			for (int i = 0; i < imax; ++i)
				axes[i] /= values[i];

			return *this;
		}

		FORCEINLINE UProfile &operator/=(const UProfile &profile)
		{
			int imax = (axes.Num() < profile.axes.Num() ? axes.Num() : profile.axes.Num());
			for (int i = 0; i < imax; ++i)
				axes[i] *= profile.axes[i];

			return *this;
		}

		FORCEINLINE UProfile operator*(float value) const
		{
			UProfile prof;
			prof.axes = axes;
			prof *= value;

			return prof;
		}

		FORCEINLINE UProfile operator*(const TArray<float> &values) const
		{
			UProfile prof;
			prof.axes = axes;
			prof *= values;

			return prof;
		}

		FORCEINLINE UProfile operator*(UProfile profile) const
		{
			UProfile prof;
			prof.axes = axes;
			prof *= profile;

			return prof;
		}

		FORCEINLINE UProfile operator/(float value) const
		{
			UProfile prof;
			prof.axes = axes;
			prof /= value;

			return prof;
		}

		FORCEINLINE UProfile operator/(const TArray<float> &values) const
		{
			UProfile prof;
			prof.axes = axes;
			prof /= values;

			return prof;
		}

		FORCEINLINE UProfile operator/(const UProfile &profile) const
		{
			UProfile prof;
			prof.axes = axes;
			prof /= profile;

			return prof;
		}

		UFUNCTION(BlueprintCallable, Category = "Axes")
		float Magnitude() const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void Normalize();

		UFUNCTION(BlueprintCallable, Category = "Axes")
		bool isNormalized() const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		int SelectAxis(float rand_value) const;

		UFUNCTION(BlueprintCallable, Category = "Axes")
		void Check(const AAdaptativeGameSettings *settings);


		FORCEINLINE friend UProfile operator*(float value, const UProfile &profile)
		{
			return profile.operator*(value);
		}


		FORCEINLINE friend UProfile operator*(const TArray<float> &values, UProfile profile)
		{
			return profile *= values;
		}
};


/*int SelectProfile(float rand_value, const FString &axis, const TArray<UProfile> &profiles)
{
	auto p = profiles[0];
	p * rand_value;
}*/
