// Fill out your copyright notice in the Description page of Project Settings.

#include "LuaEngine.h"


#include "Profile.h"


int lua_createQuestAction(lua_State *state)
{
	const char *type = luaL_checklstring(state, 1, nullptr);
	const char *target = luaL_checklstring(state, 2, nullptr);
	int nbTargets = luaL_checkinteger(state, 1);
	size_t nbytes = sizeof(UQuestAction*);
	UQuestAction **action = static_cast<UQuestAction**>(lua_newuserdata(state, nbytes));
	*action = NewObject<UQuestAction>();
	(*action)->Init(FString(ANSI_TO_TCHAR(type)), FString(ANSI_TO_TCHAR(target)), nbTargets);

	return 1;
}

int lua_createQuestStep(lua_State *state)
{
	size_t nbytes = sizeof(UQuestStep*);
	UQuestStep **step = static_cast<UQuestStep**>(lua_newuserdata(state, nbytes));
	*step = NewObject<UQuestStep>();

	return 1;
}

int lua_addQuestAction(lua_State *state)
{
	UQuestStep **step = static_cast<UQuestStep**>(lua_touserdata(state, 1));
	UQuestAction **action = static_cast<UQuestAction**>(lua_touserdata(state, 2));

	(*step)->actions.Add(*action);

	return 0;
}


int lua_createQuest(lua_State *state)
{
	size_t nbytes = sizeof(UQuest*);
	UQuest **step = static_cast<UQuest**>(lua_newuserdata(state, nbytes));
	*step = NewObject<UQuest>();

	return 1;
}

int lua_addQuestStep(lua_State *state)
{
	UQuest **quest = static_cast<UQuest**>(lua_touserdata(state, 1));
	UQuestStep **step = static_cast<UQuestStep**>(lua_touserdata(state, 2));

	(*quest)->AddStep(*step);

	return 0;
}

static const luaL_Reg qactionlib[] = {
	{ "new", lua_createQuestAction },
	{ NULL, NULL }
};

static const luaL_Reg qsteplib[] = {
	{ "new", lua_createQuestStep },
	{ "add", lua_addQuestAction },
	{ NULL, NULL }
};

static const luaL_Reg questlib[] = {
	{ "new", lua_createQuest },
	{ "add", lua_addQuestStep },
	{ NULL, NULL }
};

int lua_addQuestUtils(lua_State *state) {
	lua_newtable(state);
	luaL_setfuncs(state, qactionlib, 0);
	lua_setglobal(state, "QuestAction");

	lua_newtable(state);
	luaL_setfuncs(state, qsteplib, 0);
	lua_setglobal(state, "QuestStep");

	lua_newtable(state);
	luaL_setfuncs(state, questlib, 0);
	lua_setglobal(state, "Quest");

	return 1;
}


bool ULuaEngine::RunScript(const FString &script_file)
{
	// on lance le script lua
	if (luaL_loadfile(m_state, TCHAR_TO_ANSI(*script_file)) || lua_pcall(m_state, 0, 0, 0))
		return false;

	return true;
}


bool ULuaEngine::RunCode(const FString &script_code)
{
	// on lance le script lua
	if (luaL_loadstring(m_state, TCHAR_TO_ANSI(*script_code)) || lua_pcall(m_state, 0, 0, 0))
		return false;

	return true;
}

ULuaEngine::ULuaEngine()
{
	// on cr�er un contexte d'ex�cution de Lua
	m_state = luaL_newstate();
	// on charge les biblioth�ques standards de Lua
	luaL_openlibs(m_state);
	lua_addQuestUtils(m_state);

	initialized = m_state != nullptr;
}

ULuaEngine::~ULuaEngine()
{
	lua_close(m_state);
}



void ULuaEngine::PushAxes(const FString &name, const TArray<FString> &axes)
{
	lua_newtable(m_state);
	for (int32 i = 0, imax = axes.Num(); i < imax; ++i)
	{
		lua_pushnumber(m_state, i + 1);
		lua_pushstring(m_state, TCHAR_TO_ANSI(*(axes[i])));
		lua_settable(m_state, -3);
	}
	lua_setglobal(m_state, TCHAR_TO_ANSI(*(name)));
}


void ULuaEngine::PushProfile(UProfile *profile)
{
	lua_newtable(m_state);
	for (int32 i = 0, imax = profile->axes.Num(); i < imax; ++i)
	{
		lua_pushnumber(m_state, i + 1);
		lua_pushnumber(m_state, profile->axes[i]);
		lua_settable(m_state, -3);
	}
}

UQuest *ULuaEngine::GenerateQuest(const UQuestTemplate *qtemplate)
{
	RunCode(qtemplate->script);
	UQuest **quest = static_cast<UQuest**>(lua_touserdata(m_state, 1));

	return *quest;
}
