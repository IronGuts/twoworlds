// Fill out your copyright notice in the Description page of Project Settings.

#include "AdaptativeGameSettings.h"


AAdaptativeGameSettings::AAdaptativeGameSettings()
{
	luaEngine = NewObject<ULuaEngine>();
	luaEngine->PushAxes(FString("GAME_AXES"), axes);
	minAxisValue = 0.00001f;
}

AAdaptativeGameSettings::~AAdaptativeGameSettings()
{
}


float AAdaptativeGameSettings::GetMinAxisValue() const
{
	return minAxisValue;
}

void AAdaptativeGameSettings::SetMinAxisValue(float value)
{
	if (value > 0.0f)
		minAxisValue = value;
}

int AAdaptativeGameSettings::GetIndex(const FString &axis) const
{
	return axes.IndexOfByKey(axis);
}

bool AAdaptativeGameSettings::HasAxis(const FString &axis) const
{
	return GetIndex(axis) != INDEX_NONE;
}


void AAdaptativeGameSettings::AddAxis(const FString &axis)
{
	axes.Add(axis);
}

void AAdaptativeGameSettings::RemoveAxis(const FString &axis)
{
	axes.Remove(axis);
}


UProfile *AAdaptativeGameSettings::GetActionProfiles(const FString &action)
{
	return (actionProfiles.Contains(action) ? actionProfiles[action] : nullptr);
}


UQuest *AAdaptativeGameSettings::GenerateQuest(const UProfile *profile)
{
	float r = FMath::RandRange(0.0f, 1.0f);
	int axis = profile->SelectAxis(r);

	
	TArray<float> proba;
	{ // Génération des probabilités
		float norm = 0.0f;
		for (int32 i = 0, imax = questTemplates.Num(); i < imax; ++i) // Création du vecteur de probabilités en fonction de l'axe sélectionné
		{
			float val = questTemplates[i]->profile->axes[axis];
			if (val == 0.0f)
				continue;

			proba.Add(val);
			norm += FMath::Pow(val, 2.0f);
		}

		if (proba.Num() == 0)
			return nullptr;

		norm = FMath::Sqrt(norm);
		for (int32 i = 0, imax = proba.Num(); i < imax; ++i)
			proba[i] = proba[i] / norm;
	}

	int q = 0;
	r = FMath::RandRange(0.0f, 1.0f);
	float accum = proba[0];
	while (r > accum && q < proba.Num())
	{
		++q;
		accum += proba[q];
	}

	if (q >= proba.Num())
		return nullptr;


	return luaEngine->GenerateQuest(questTemplates[q]);
}

