// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Quest.h"
#include "Profile.h"


#include "QuestTemplate.generated.h"

#define private public
#define protected public

/**
 * 
 */
UCLASS(BlueprintType)
class MAIN_API UQuestTemplate : public UObject
{
	GENERATED_BODY()

	public:

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Definition")
		FString name;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Definition")
		UProfile *profile;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Script")
		FString script;


	public:
		UQuestTemplate();
		~UQuestTemplate();
};
