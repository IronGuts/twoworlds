// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "QuestStep.h"
#include "Profile.h"


#include "Quest.generated.h"

#define private public
#define protected public

UCLASS(BlueprintType)
class MAIN_API UQuest : public UObject
{
	GENERATED_BODY()

	public:
		
		UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient, Category = "Definition")
		FString name;

		UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient, Category = "Definition")
		UProfile *profile;

		UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient, Category = "Definition")
		TArray<UQuestStep*> steps;

		UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient, Category = "State")
		int currentStep;

		UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient, Category = "Definition")
		bool mandatory;

	public:

		UQuest();
		~UQuest();

		UFUNCTION(BlueprintCallable, Category = "Init")
		void Init(const FString &questName,
				  const TArray<UQuestStep*> &questSteps,
				  int questCurrentStep = 0,
				  bool isMandatory = true);

		UFUNCTION(BlueprintCallable, Category = "Profile")
		UProfile * const GetProfile() const { return profile; }

		UFUNCTION(BlueprintCallable, Category = "Quest")
		const FString &GetName() const { return name; }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		const TArray<UQuestStep*> &GetSteps() const { return steps; }

		UFUNCTION(BlueprintCallable, Category = "State")
		int GetCurrentStepIndex() const { return currentStep; }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		UQuestStep * const GetConstCurrentStep(int index) const { return steps[index]; }
		
		UFUNCTION(BlueprintCallable, Category = "Steps")
		UQuestStep *GetCurrentStep(int index) { return steps[index]; }

		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsMandatory() const { return mandatory; }

		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsDone() const;

		UFUNCTION(BlueprintCallable, Category = "Profile")
		void SetProfile(UProfile *val) { profile = val; }

		UFUNCTION(BlueprintCallable, Category = "Quest")
		void SetName(const FString &val) { name = val; }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		void SetSteps(TArray<UQuestStep*> val) { steps = val; Check(); }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		void AddStep(UQuestStep* val) { steps.Add(val); }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		void InsertStep(UQuestStep* val, int index) { steps.Insert(val, index); }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		void AddSteps(TArray<UQuestStep*> val) { steps += val; }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		void RemoveStep(UQuestStep* val) { steps.Remove(val); }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		void RemoveStepAt(int index) { steps.RemoveAt(index); }

		UFUNCTION(BlueprintCallable, Category = "Steps")
		void ClearSteps(int index) { steps.Empty(); }

		UFUNCTION(BlueprintCallable, Category = "State")
		void SetCurrentStepIndex(int val) { currentStep = val; }
		
		UFUNCTION(BlueprintCallable, Category = "State")
		void SetMandatory(bool val) { mandatory = val; }

		UFUNCTION(BlueprintCallable, Category = "State")
		bool HasActions() const;

		UFUNCTION(BlueprintCallable, Category = "State")
		bool HasUndoneActions() const;

		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsStepHasUndoneActions(int step) const;

		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsValid() const;

		UFUNCTION(BlueprintCallable, Category = "State")
		void Check();

		UFUNCTION(BlueprintCallable, Category = "State")
		void OnActionDone(const FString &action, const FString &target);

		FORCEINLINE UQuestStep * const operator[](int index) const { return steps[index]; }
		FORCEINLINE UQuestStep *operator[](int index) { return steps[index]; }
		FORCEINLINE operator bool() const { return IsDone(); }
		FORCEINLINE UQuest &operator<<(UQuestStep *step) { steps.Add(step); return *this; }
};