// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "QuestAction.generated.h"

#define private public
#define protected public

UCLASS(BlueprintType)
class MAIN_API UQuestAction : public UObject
{
	GENERATED_BODY()

	public:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Type")
		FString actionType;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
		FString target;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
		int numberOfTargets;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Target")
		int numberOfDoneTargets;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Type")
		bool mandatory;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool done;

	public:

		UQuestAction();
		~UQuestAction();


		UFUNCTION(BlueprintCallable, Category = "Init")
		void Init(const FString &Type = "", const FString &TargetType = "",
			int NbOfTargets = -1, int NbOfDoneTargets = -2,
			bool IsMandatory = false, bool IsDone = false);

		UFUNCTION(BlueprintCallable, Category = "Init")
		void InitFromAction(const UQuestAction *q) { Init(q->actionType, q->target, q->numberOfTargets, q->numberOfDoneTargets, q->mandatory, q->done); }

		UFUNCTION(BlueprintCallable, Category = "Type")
		const FString &GetActionType() const { return actionType; }
		
		UFUNCTION(BlueprintCallable, Category = "Target")
		const FString &getTarget() const { return target; }
		
		UFUNCTION(BlueprintCallable, Category = "Target")
		int GetNumberOfTargets() const { return numberOfTargets; }
		
		UFUNCTION(BlueprintCallable, Category = "Target")
		int GetNumberOfDoneTargets() const { return numberOfDoneTargets; }
		
		UFUNCTION(BlueprintCallable, Category = "Type")
		bool IsMandatory() const { return mandatory; }
		
		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsDone() const { return done; }

		UFUNCTION(BlueprintCallable, Category = "Type")
		void SetActionType(const FString &val) { actionType = val; }
		
		UFUNCTION(BlueprintCallable, Category = "Target")
		void SetTarget(const FString &val) { target = val; }
		
		UFUNCTION(BlueprintCallable, Category = "Target")
		void SetNumberOfTargets(int val) { numberOfTargets = val; }
		
		UFUNCTION(BlueprintCallable, Category = "Target")
		void SetNumberOfDoneTargets(int val) { numberOfDoneTargets = val; }
		
		UFUNCTION(BlueprintCallable, Category = "Type")
		void SetMandatory(bool val) { mandatory = val; }
		
		UFUNCTION(BlueprintCallable, Category = "State")
		void SetDone(bool val) { done = val; }

		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsValid() const { return !actionType.IsEmpty() && !target.IsEmpty() && numberOfTargets > 0 && numberOfDoneTargets >= 0; }
		
		UFUNCTION(BlueprintCallable, Category = "State")
		void TargetDone() { MultipleTargetDone(1); }
		
		UFUNCTION(BlueprintCallable, Category = "State")
		void MultipleTargetDone(int number) { numberOfDoneTargets += number; Check(); }

		UFUNCTION(BlueprintCallable, Category = "State")
		void TargetUndone() { MultipleTargetUndone(1); }

		UFUNCTION(BlueprintCallable, Category = "State")
		void MultipleTargetUndone(int number) { MultipleTargetDone(-number); }

		UFUNCTION(BlueprintCallable, Category = "State")
		void Check() { done = (numberOfTargets <= numberOfDoneTargets); }

		FORCEINLINE operator bool() const { return IsValid(); }
		FORCEINLINE UQuestAction &operator++() { TargetDone(); return *this; }
		FORCEINLINE UQuestAction operator++(int) { UQuestAction q; q.InitFromAction(this); TargetDone(); return q; }
		FORCEINLINE UQuestAction &operator+=(int number) { MultipleTargetDone(number); return *this; }
		FORCEINLINE UQuestAction operator+(int number) const { UQuestAction q; q.InitFromAction(this); q.MultipleTargetDone(number); return q; }

		FORCEINLINE UQuestAction &operator--() { TargetUndone(); return *this; }
		FORCEINLINE UQuestAction operator--(int) { UQuestAction q; q.InitFromAction(this); TargetUndone(); return q; }
		FORCEINLINE UQuestAction &operator-=(int number) { MultipleTargetUndone(number); return *this; }
		FORCEINLINE UQuestAction operator-(int number) const { UQuestAction q; q.InitFromAction(this); q.MultipleTargetUndone(number); return q; }
};
