// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <lua/lua.hpp>


#include <sstream>

#include <vector>
#include <map>
#include <unordered_map>

#include "QuestTemplate.h"


#include "LuaEngine.generated.h"

#define private public
#define protected public

class UProfile;

#pragma region Global Run

bool runScript(lua_State * state, const char *script_file);


bool runCode(lua_State * state, const char *script_code);

#pragma endregion


int lua_createQuestAction(lua_State *state);

int lua_createQuestStep(lua_State *state);


UCLASS(BlueprintType)
class MAIN_API ULuaEngine : public UObject
{
	GENERATED_BODY()
	
	public:
		lua_State *m_state;

		UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient, Category = "State")
		bool initialized;


	public:
		
		ULuaEngine();
		~ULuaEngine();


		UFUNCTION(BlueprintCallable, Category = "Run")
		bool RunScript(const FString &script_file);

		UFUNCTION(BlueprintCallable, Category = "Run")
		bool RunCode(const FString &script_code);


		void PushAxes(const FString &name, const TArray<FString> &axes);
		
		void PushProfile(UProfile *profile);

		UQuest *GenerateQuest(const UQuestTemplate *qtemplate);



		/*template<typename T>
		FORCEINLINE bool Is(int index)
		{
			return false;
		}

		template<>
		FORCEINLINE bool Is<bool>(int index)
		{
			return lua_isboolean(m_state, index) != 0;
		}

		template<>
		FORCEINLINE bool Is<int32>(int index)
		{
			return lua_isinteger(m_state, index) != 0;
		}

		template<>
		FORCEINLINE bool Is<float>(int index)
		{
			return lua_isnumber(m_state, index) != 0;
		}

		template<>
		FORCEINLINE bool Is<double>(int index)
		{
			return lua_isnumber(m_state, index) != 0;
		}*/

		/*template<>
		FORCEINLINE bool Is<LuaCallableFunction>(int index)
		{
			return lua_isfunction(m_state, index) != 0;
		}*/

		/*template<>
		FORCEINLINE bool Is<FString>(int index)
		{
			return lua_isstring(m_state, index) != 0;
		}

		template<>
		FORCEINLINE bool Is<FText>(int index)
		{
			return lua_isstring(m_state, index) != 0;
		}

		template<>
		FORCEINLINE bool Is<FName>(int index)
		{
			return lua_isstring(m_state, index) != 0;
		}*/

		/*template<>
		FORCEINLINE bool Is<FChar*>(int index)
		{
			return lua_isstring(m_state, index) != 0;
		}

		template<>
		FORCEINLINE bool Is<char*>(int index)
		{
			return lua_isstring(m_state, index) != 0;
		}*/

		/*template<typename T>
		FORCEINLINE bool getGlobal(const FString &name, T &value)
		{
			lua_getglobal(state, TCHAR_TO_ANSI(*script_code));
			FDLua::get<T>(m_state, value, -1, true);
		}*/

		/*template<typename T>
		FORCEINLINE bool pushGlobal(const FString &name, T &value)
		{
			lua_getglobal(state, TCHAR_TO_ANSI(*script_code));
			FDLua::get<T>(m_state, value, -1, true);
		}*/
};
