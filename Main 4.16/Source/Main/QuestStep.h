// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"

#include "QuestAction.h"

#include "QuestStep.generated.h"

#define private public
#define protected public

UCLASS(BlueprintType)
class MAIN_API UQuestStep : public UObject
{
	GENERATED_BODY()

	public:
		
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actions")
		TArray<UQuestAction*> actions;
	
	public:
		UQuestStep();
		~UQuestStep();

		UFUNCTION(BlueprintCallable, Category = "Actions")
		const TArray<UQuestAction*> &GetActions() const { return actions; }
		
		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsMandatory() const;
		
		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsDone() const;

		UFUNCTION(BlueprintCallable, Category = "State")
		bool IsComplete() const;

		UFUNCTION(BlueprintCallable, Category = "Actions")
		void SetActions(TArray<UQuestAction*> val) { actions = val; }
		
		UFUNCTION(BlueprintCallable, Category = "Actions")
		bool HasActions() const { return actions.Num() > 0; }

		UFUNCTION(BlueprintCallable, Category = "Actions")
		bool Contains(const FString &action, const FString &target) const;

		UFUNCTION(BlueprintCallable, Category = "Actions")
		int32 IndexOf(const FString &action, const FString &target) const;

		UFUNCTION(BlueprintCallable, Category = "Actions")
		int32 OnActionDone(const FString &action, const FString &target);

		FORCEINLINE const UQuestAction &operator[](int index) const { return *(actions[index]); }
		FORCEINLINE UQuestAction &operator[](int index) { return *(actions[index]); }
		FORCEINLINE operator bool() const { return IsDone(); }
		FORCEINLINE UQuestStep &operator<<(UQuestAction *step) { actions.Add(step); return *this; }
};
