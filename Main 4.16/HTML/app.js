var app = angular.module('myApp', []);

app.controller('MainController',[ '$scope', '$http', '$sce' , function($scope, $http, $sce) {
    $scope.boolLoad = false;
    $scope.batman = false;
    $scope.vueJoueur = false;
    $scope.vueQuest = false;
    $scope.joueur = [];
    $scope.axe;
    $scope.quetes = [];
    $scope.quete;
    $scope.urlJson = "";

    $scope.quete = {
        "name": "Clear zone",
        "profile": {
            "killer": "0.8944271909999159",
            "explorer": "0.4472135954999579",
            "collecter": "0.0",
            "socializer": "0.0"
        },
        "script": "print(\\\" her comes some lua script that will generate the quest's steps and maybe the profile modifier (which is a profile)\")"
    };

    $scope.joueur = {
        "name": "Iron Guts",
        "profile": { "killer": "0.25" ,
            "explorer": "0.25",
            "collecter": "0.25",
             "socializer": "0.25" },
        "experience": 0,
        "lifepoints": 100,
        "manapoints": 100
    };

    $scope.Robin = function(ind)
    {
        console.log($scope.joueur.profile);
        console.log($scope.joueur.profile[ind]);
        $scope.batman = !$scope.batman;
        $scope.axe = $scope.joueur.profile[ind];
        console.log($scope.axe);

    }

    $scope.ChooseQuest = function (ind)
    {
        $scope.quete = $scope.quetes[ind];
    }

    $scope.Joker= function(ind)
    {
        $scope.batman = !$scope.batman;
        $scope.quete = $scope.quetes[ind];
    }

    $scope.Test = function ()
    {
        $scope.VoirJoueur();
        $scope.TrueBool("C:\Users\Tristan\Documents\twoworlds\Main 4.16\HTML\AdaptativeGame\players.json");
    }

    $scope.SeeQuestsFromJson = function ()
    {
        $scope.quetes = [
            {
                "name": "Clear zone",
		
                "profile": {
                    "killer" : 0.8944271909999159,
                    "explorer" : 0.4472135954999579,
                    "collecter" : 0.0,
                    "socializer" : 0.0
                },
			
			"script": "print(\"her comes some lua script that will generate the quest's steps and maybe the profile modifier (which is a profile)\")"
            }];
        $scope.boolLoad = true;
    }

    $scope.UpdateQuest = function (name, killer, socializer, explorer, collecter, script)
    {
        $scope.quete.name = name;
        $scope.quete.profile.killer = killer;
        $scope.quete.profile.socializer = socializer;
        $scope.quete.profile.explorer = explorer;
        $scope.quete.profile.collecter = collecter;
        $scope.quete.script = script;
        $scope.quetes[0] = $scope.quete;
        $scope.batman = !$scope.batman;
     
    }

    $scope.AddQuest = function ()
    {
        $scope.quetes.push({
            "name": "New Quest",
            "profile": {
                "killer": "0.0",
                "explorer": "0.0",
                "collecter": "0.0",
                "socializer": "0.0"
            },
            "script": ""
        });

    }

    $scope.SuprQuest = function(ind)
    {
        $scope.quetes.splice();
    }

    $scope.TrueBool = function(url)
    {
        console.log(url);
        if (url != "") {
            $sce.trustAsResourceUrl(url);
            $scope.urlJson = "C:/Users/Tristan/Documents/twoworlds/Main 4.16/HTML/AdaptativeGame/players.json";
            //url = "C:/Users/Tristan/Documents/twoworlds/Main 4.16/HTML/AdaptativeGame/players.json"; //C:\Users\Tristan\Documents\twoworlds\Main 4.16\HTML\AdaptativeGame\players.json
            $scope.boolLoad = true;
            $http.jsonp(url, { jsonpCallbackParam: 'callback' })
                .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
        }
        else
        {
            //pour debug
            console.log(url);
            console.log($scope.urlJson);
        }
    }

    $scope.VoirJoueur = function () {
        if (!$scope.vueJoueur)
            $scope.vueJoueur = true;
        else
            $scope.vueJoueur = false;
        $scope.vueQuest = false;
    }

    $scope.VoirQuests = function () {
        if (!$scope.vueQuest)
            $scope.vueQuest = true;
        else
            $scope.vueQuest = false;
        $scope.vueJoueur = false;
    }

    $scope.AddAxis = function()
    {

    }

}]);




function addRow(elmt, value) {
    if (value != "") {
        var tr = document.createElement('tr');
        elmt.appendChild(tr);

        var td = document.createElement('td');
        tr.appendChild(td);
        var tdText = document.createTextNode(value);
        td.appendChild(tdText);
    }
}

function addRowAndValue(elmt, value) {
    if (value != "") {
        var tr = document.createElement('tr');
        elmt.appendChild(tr);

        var nbRows = elmt.getElementsByTagName('tr').lenght;
        nbRows = "val" + nbRows;

        var td = document.createElement('td');
        var tdText = document.createTextNode(value);
        td.appendChild(tdText);

        var td2 = document.createElement('td');
        var td2Text = document.createElement("input");
        td2Text.setAttribute("id",nbRows);

        td2.appendChild(td2Text);
        tr.appendChild(td);
        tr.appendChild(td2);
    }
}